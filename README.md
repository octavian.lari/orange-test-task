To start the app, follow each of the `README.md` instructions
- for back-end: `source/back-end/README.md`
- for front-end: `source/front-end/README.md`
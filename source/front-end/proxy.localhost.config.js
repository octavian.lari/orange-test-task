const PROXY_CONFIG = [{
  context: ['/graphql'],
  target: 'http://localhost:3000',
  secure: false,
  logLevel: "debug",
  proxyTimeout: 0,
  timeout: 0,
}]

module.exports = PROXY_CONFIG;

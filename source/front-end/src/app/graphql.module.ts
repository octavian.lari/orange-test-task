import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ApolloBoost, ApolloBoostModule } from 'apollo-angular-boost';
import { environment } from 'src/environments/environment.prod';


@NgModule({
  exports: [
    HttpClientModule,
    ApolloBoostModule,
  ],
})
export class GraphqlModule {
  constructor(
    apolloBoost: ApolloBoost,
  ) {
    apolloBoost.create({
      uri: environment.graphqlUri,
    });
  }
}

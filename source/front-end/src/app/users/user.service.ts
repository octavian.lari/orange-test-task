import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { User } from './models';

const mockUserData: Array<User> = [
  {
    _id: '0',
    status: 0,
    firstName: 'Carmela',
    lastName: 'Neal',
    email: 'carmela.neal@orange.us',
  },
  {
    _id: '1',
    status: 1,
    firstName: 'Guadalupe',
    lastName: 'Bonner',
    email: 'guadalupe.bonner@orange.name',
  },
  {
    _id: '2',
    status: 1,
    firstName: 'Wilma',
    lastName: 'Patrick',
    email: 'wilma.patrick@orange.io',
  },
  {
    _id: '3',
    status: 1,
    firstName: 'Jodi',
    lastName: 'Murray',
    email: 'jodi.murray@orange.biz',
  },
  {
    _id: '4',
    status: 1,
    firstName: 'Briggs',
    lastName: 'Gallagher',
    email: 'briggs.gallagher@orange.net',
  },
  {
    _id: '5',
    status: 0,
    firstName: 'Marguerite',
    lastName: 'Boyle',
    email: 'marguerite.boyle@orange.biz',
  },
  {
    _id: '6',
    status: 0,
    firstName: 'Kelsey',
    lastName: 'Hodges',
    email: 'kelsey.hodges@orange.com',
  },
  {
    _id: '7',
    status: 2,
    firstName: 'Farmer',
    lastName: 'Webster',
    email: 'farmer.webster@orange.me',
  },
  {
    _id: '8',
    status: 2,
    firstName: 'Maldonado',
    lastName: 'Barnett',
    email: 'maldonado.barnett@orange.org',
  },
  {
    _id: '9',
    status: 0,
    firstName: 'Chelsea',
    lastName: 'Higgins',
    email: 'chelsea.higgins@orange.co.uk',
  },
  {
    _id: '10',
    status: 0,
    firstName: 'Owens',
    lastName: 'Rush',
    email: 'owens.rush@orange.tv',
  },
  {
    _id: '11',
    status: 1,
    firstName: 'Randall',
    lastName: 'Campbell',
    email: 'randall.campbell@orange.info',
  },
  {
    _id: '12',
    status: 2,
    firstName: 'Serena',
    lastName: 'Garza',
    email: 'serena.garza@orange.us',
  },
  {
    _id: '13',
    status: 2,
    firstName: 'Therese',
    lastName: 'Mason',
    email: 'therese.mason@orange.name',
  },
  {
    _id: '14',
    status: 1,
    firstName: 'Waters',
    lastName: 'Avery',
    email: 'waters.avery@orange.io',
  },
  {
    _id: '15',
    status: 2,
    firstName: 'Sarah',
    lastName: 'Jensen',
    email: 'sarah.jensen@orange.biz',
  },
  {
    _id: '16',
    status: 1,
    firstName: 'Collins',
    lastName: 'Woodward',
    email: 'collins.woodward@orange.net',
  },
  {
    _id: '17',
    status: 0,
    firstName: 'Monique',
    lastName: 'Elliott',
    email: 'monique.elliott@orange.biz',
  },
  {
    _id: '18',
    status: 1,
    firstName: 'Thompson',
    lastName: 'Hunt',
    email: 'thompson.hunt@orange.com',
  },
  {
    _id: '19',
    status: 2,
    firstName: 'Bradley',
    lastName: 'Hood',
    email: 'bradley.hood@orange.me',
  },
];

@Injectable({
  providedIn: 'root',
})
export class UserService {

  constructor(
    private http: HttpClient,
  ) { }

  getUsers(): Observable<Array<User>> {
    return of(mockUserData).pipe(
      delay(2000),
    );
  }

  addUser(user: User): Observable<User> {
    return of(user).pipe(
      delay(2000),
    );
  }

  editUser(user: User): Observable<User> {
    return of(user).pipe(
      delay(2000),
    );
  }

  deleteUser(user: User): Observable<null> {
    return of(null).pipe(
      delay(2000),
    );
  }

}

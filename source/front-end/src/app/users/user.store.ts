import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { List } from 'immutable';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from './models/user.interface';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root',
})
export class UserStore {

  private usersSubject: BehaviorSubject<List<User>> = new BehaviorSubject(List([]));
  usersSource: Observable<List<User>> = this.usersSubject.asObservable();

  constructor(
    private userService: UserService,
    private snackbar: MatSnackBar,
  ) { }

  loadUsers() {
    this.userService.getUsers().subscribe(users => {
      this.usersSubject.next(List(users));
    });
  }

  addUser(user: User) {
    this.snackbar.open('Adding new user...');

    this.userService.addUser(user).subscribe(newUser => {
      this.usersSubject.next(this.rawList.push(newUser));
      this.snackbar.open('User added');
    });
  }

  deleteUser(user: User) {
    this.snackbar.open('Deleting user...');

    this.userService.deleteUser(user).subscribe(() => {
      const index: number = this.getUserIndex(user);

      if (index !== -1) {
        this.usersSubject.next(this.rawList.remove(index));
        this.snackbar.open('User deleted');
      }
    });
  }

  editUser(user: User) {
    this.snackbar.open('Editing user...');
    this.userService.editUser(user).subscribe(newUser => {
      const index: number = this.getUserIndex(newUser);

      if (index !== -1) {
        this.usersSubject.next(this.rawList.set(index, newUser));
        this.snackbar.open('User edited');
      }
    });
  }

  private getUserIndex(user: User): number {
    return this.rawList.findIndex(u => u._id === user._id);
  }

  private get rawList(): List<User> {
    return this.usersSubject.getValue();
  }

}

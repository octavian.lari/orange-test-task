import { Component, OnInit, Optional, Inject } from '@angular/core';
import { User } from '../models';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'ort-user-delete-confirm',
  templateUrl: './user-delete-confirm.component.html',
  styleUrls: ['./user-delete-confirm.component.scss'],
})
export class UserDeleteConfirmComponent implements OnInit {

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: User,
  ) { }

  ngOnInit() {}

}

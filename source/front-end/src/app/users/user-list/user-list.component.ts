import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Apollo, gql, QueryRef } from 'apollo-angular-boost';
import { Observable } from 'rxjs';
import { filter, map, switchMap, tap } from 'rxjs/operators';
import { AddUserDialogComponent } from '../add-user/add-user.dialog';
import { User } from '../models';
import { UserStore } from '../user.store';


@Component({
  selector: 'ort-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserListComponent implements OnInit {

  private userListWatchQuery: QueryRef<any>;

  usersList: Observable<Array<User>>;

  private usersQuery = gql`
    query users {
      users {
        firstName
        lastName
        status
        email
        _id
      }
    }
  `;

  private userEditMutation = gql`
    mutation editUser($input: EditUserInput!) {
      editUser(input: $input) {
        _id
      }
    }
  `;

  private userCreateMutation = gql`
    mutation createUser($input: UserInput!) {
      createUser(input: $input) {
        _id
      }
    }
  `;

  private userDeleteMutation = gql`
    mutation deleteUser($input: String!) {
      deleteUser(input: $input) {
        _id
      }
    }
  `;

  constructor(
    public apollo: Apollo,
    public userStore: UserStore,
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.userListWatchQuery = this.apollo.watchQuery({ query: this.usersQuery });

    this.usersList = this.userListWatchQuery.valueChanges.pipe(
      map((response: any) => response.data.users as Array<User>),
    );
  }

  addUser() {
    let addMutate = (user: User) => this.apollo.mutate({
      mutation: this.userCreateMutation,
      variables: {
        input: user,
      },
    });

    this.dialog.open(AddUserDialogComponent).afterClosed().pipe(
      filter(Boolean),
      tap(() => this.snackbar.open('Adding new user...', null, { duration: Infinity })),
      switchMap(addMutate),
      switchMap(() => this.userListWatchQuery.refetch()),
    ).subscribe(() => {
      this.snackbar.open('User added');
    }, e => {
      this.snackbar.open(e);
    });
  }

  deleteUser(user: User) {
    let deleteUser = (id: string) => this.apollo.mutate({
      mutation: this.userDeleteMutation,
      variables: {
        input: id,
      },
    });

    this.snackbar.open('Removing user...');

    deleteUser(user._id).pipe(
      switchMap(() => this.userListWatchQuery.refetch()),
    ).subscribe(() => {
      this.snackbar.open('User removed');
    }, e => {
      this.snackbar.open(e);
    });
  }

  editUser(user: User) {
    let addMutate = (inputUser: User) => this.apollo.mutate({
      mutation: this.userEditMutation,
      variables: {
        input: inputUser,
      },
    });

    this.dialog.open(AddUserDialogComponent, {
      data: user,
    }).afterClosed().pipe(
      filter(Boolean),
      tap(() => this.snackbar.open('Updating user...', null, { duration: Infinity })),
      switchMap(addMutate),
      switchMap(() => this.userListWatchQuery.refetch()),
    ).subscribe(() => {
      this.snackbar.open('User updated');
    }, e => {
      this.snackbar.open(e);
    });
  }

  trackUsersByID(index: number, user: User) {
    return user._id;
  }

}

import { ChangeDetectionStrategy, Component, Inject, OnInit, Optional } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from '../models';

declare type DialogWriteMode = 'create' | 'edit';

@Component({
  selector: 'ort-add-user',
  templateUrl: './add-user.dialog.html',
  styleUrls: ['./add-user.dialog.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddUserDialogComponent implements OnInit {

  userForm: FormGroup;
  dialogWriteMode: DialogWriteMode;
  dialogWriteModeText: string;

  constructor(
    public dialogRef: MatDialogRef<AddUserDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: User,
  ) {
    this.dialogWriteMode = this.data === null ? 'create' : 'edit';
    this.dialogWriteModeText = this.dialogWriteMode === 'create' ? 'Create' : 'Edit';
  }

  ngOnInit() {
    this.initFormGroup(this.data);
  }

  private initFormGroup(user: Partial<User>) {
    user = user || {};

    this.userForm = new FormGroup({
      firstName: new FormControl(user.firstName, { validators: [Validators.required] }),
      lastName: new FormControl(user.lastName, { validators: [Validators.required] }),
      email: new FormControl(user.email, { validators: [Validators.required, Validators.email] }),
      status: new FormControl(user.status, { validators: [Validators.required] }),
    });

    if (user._id) {
      this.userForm.addControl('_id', new FormControl(user._id));
    }
  }

}

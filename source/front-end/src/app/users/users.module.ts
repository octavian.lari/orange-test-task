import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { AddUserDialogComponent } from './add-user/add-user.dialog';
import { UserDeleteConfirmComponent } from './user-delete-confirm/user-delete-confirm.component';
import { UserListItemComponent } from './user-list-item/user-list-item.component';
import { UserListComponent } from './user-list/user-list.component';
import { UsersRoutingModule } from './users.routing';

@NgModule({
  declarations: [
    UserListComponent,
    UserListItemComponent,
    AddUserDialogComponent,
    UserDeleteConfirmComponent,
  ],
  entryComponents: [
    AddUserDialogComponent,
    UserDeleteConfirmComponent,
  ],
  imports: [
    UsersRoutingModule,
    CommonModule,
    MatTooltipModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatSelectModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    ReactiveFormsModule,
  ],
})
export class UsersModule { }

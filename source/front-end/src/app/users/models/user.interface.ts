import { UserStatus } from './user-status.enum';

export interface User {
  _id: string;
  firstName: string;
  lastName: string;
  status: UserStatus;
  email: string;
}

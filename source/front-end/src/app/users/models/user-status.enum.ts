export enum UserStatus {
  Active = 0,
  Blocked = 1,
  Expired = 2,
}

import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { filter } from 'rxjs/operators';
import { User, UserStatus } from '../models';
import { UserDeleteConfirmComponent } from '../user-delete-confirm/user-delete-confirm.component';

@Component({
  selector: 'ort-user-list-item',
  templateUrl: './user-list-item.component.html',
  styleUrls: ['./user-list-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserListItemComponent implements OnInit {

  @Input() user: User;
  @Output() delete: EventEmitter<User> = new EventEmitter();
  @Output() edit: EventEmitter<User> = new EventEmitter();

  constructor(
    private dialog: MatDialog,
  ) { }

  ngOnInit() { }

  onDelete() {
    this.dialog.open(UserDeleteConfirmComponent, {
      disableClose: false,
      data: this.user,
    }).afterClosed().pipe(
      filter(Boolean),
    ).subscribe(() => {
      this.delete.emit(this.user);
    });
  }

  onEdit() {
    this.edit.emit(this.user);
  }

  get fullName(): string {
    return `${this.user.firstName} ${this.user.lastName}`;
  }

  get textStatus(): string {
    return UserStatus[this.user.status];
  }

}

## Prerequisites

- [ ] **Backend Server is running!**
- [ ] `Node.js >=10.0.0`
- [ ] `npm >=6.0.0`
- [ ] __optional__ `@angular/cli` to run `ng` command

## Starting server

- Run `npm install`
- Run `npm run start`
- Navigate to `http://localhost:4200/`


import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { UserTypeDTO } from './dto/create-user.dto';
import { EditUserInput } from './inputs/edit-user.input';
import { UserInput } from './inputs/user.input';
import { UsersService } from './users.service';

@Resolver()
export class UsersResolver {

  constructor(
    private readonly usersService: UsersService,
  ) { }

  @Query(() => [UserTypeDTO])
  async users() {
    return this.usersService.findAll();
  }

  @Mutation(() => UserTypeDTO)
  async createUser(@Args('input') input: UserInput) {
    return this.usersService.create(input);
  }

  @Mutation(() => UserTypeDTO)
  async editUser(@Args('input') input: EditUserInput) {
    return this.usersService.update(input);
  }

  @Mutation(() => UserTypeDTO)
  async deleteUser(@Args('input') input: string) {
    return this.usersService.delete(input);
  }

}

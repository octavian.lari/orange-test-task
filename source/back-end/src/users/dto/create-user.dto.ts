import { Field, ID, Int, ObjectType } from 'type-graphql';

@ObjectType()
export class UserTypeDTO {

  @Field(() => ID)
  readonly _id: string;
  @Field()
  readonly firstName: string;
  @Field()
  readonly lastName: string;
  @Field(() => Int)
  readonly status: number;
  @Field()
  readonly email: string;
}

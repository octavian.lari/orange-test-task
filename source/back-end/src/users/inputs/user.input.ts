import { Field, InputType, Int } from 'type-graphql';

@InputType()
export class UserInput {
  @Field()
  readonly firstName: string;
  @Field()
  readonly lastName: string;
  @Field(() => Int)
  readonly status: number;
  @Field()
  readonly email: string;
}

import { Field, ID, InputType } from 'type-graphql';
import { UserInput } from './user.input';

@InputType()
export class EditUserInput extends UserInput {
  @Field(() => ID)
  readonly _id: string;
}

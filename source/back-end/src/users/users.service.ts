import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { EditUserInput } from './inputs/edit-user.input';
import { UserInput } from './inputs/user.input';
import { User } from './interfaces/user.interface';

@Injectable()
export class UsersService {
  constructor(@InjectModel('User') private readonly userModel: Model<User>) {}

  async create(userInput: UserInput): Promise<User> {
    const createdUser = new this.userModel(userInput);
    return await createdUser.save();
  }

  async findAll(): Promise<User[]> {
    return await this.userModel.find().exec();
  }

  async findOne(userInput: EditUserInput): Promise<User> {
    return await this.userModel.findOne({ _id: userInput._id });
  }

  async delete(id: string): Promise<User> {
    return await this.userModel.findByIdAndRemove(id);
  }

  async update(userInput: EditUserInput): Promise<User> {
    const {_id, ...rest } = userInput;
    return this.userModel.findOneAndUpdate({ _id }, rest, { new: true });
  }
}

## Prerequisites

- [ ] `Node.js >=10.0.0`
- [ ] `npm >=6.0.0`
- [ ] `MongoDB >=4.0.0`

## Starting server

- Run `npm install`
- Run `npm run start:dev`
- __optional__ Navigate to `http://localhost:3000/graphql` for `GraphQL` playground
